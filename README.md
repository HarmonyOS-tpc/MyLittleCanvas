# MyLittleCanvas

🎨 Need to create a custom component ?

You don't know how to use Canvas, use MyLittleCanvas instead !

# Used

```groovy
dependencies{
   implementation 'io.openharmony.tpc.thirdlib:MyLittleCanvas:1.0.2'
}
```

# Examples

<img src="medias/example/sample_arc.gif"/>
<img src="medias/example/dots_sample.gif"/>
<img src="medias/example/slider_sample.gif"/>
<img src="medias/example/sample_tree.gif"/>

# Available shapes

**TODO : NEED TO FILL THOSE DOCUMENTATIONS**

| Shapes    | link       |
|-----------|------------|
| Rect      | [documentation](documentation/RectShape.md)  |
| Circle    | [documentation](documentation/CircleShape.md)  |
| Text      | [documentation](documentation/TextShape.md)  |
| Arc       | [documentation](documentation/ArcShape.md)  |
| Line      | [documentation](documentation/LineShape.md)  |
| Triangle  | [documentation](documentation/TriangleShape.md)  |
| Drawable  | [documentation](documentation/DrawableShape.md)  |
| Path      | [documentation](documentation/PathShape.md)  |

# Animation

Follow the example of [SwitchView](entry/src/main/java/canvastoolbox/florent37/github/com/canvastoolbox/views/SwitchView.java)

Shape animations are executed by an instance of `ShapeAnimator` attached to your component

```java
private final ShapeAnimator shapeAnimator = new ShapeAnimator(this);
```

All animated methods of shapes are wrapped into the method `.animate()`

For example, for a `CircleShape`, you can animate his position (centerX) using
```java
myCircleShape.animate().centerXTo(15);
```

Then use your `ShapeAnimator` to execute this animation

```java
shapeAnimator.play(myCircleShape.animate().centerXTo(15))
    .setDuration(500)
    .start()
```

<img src="medias/example/move_center_x.gif"/>

#### .top(values)

This method will change the shape `top` values, **ignoring its previous height**

For example, for a Rect `[left: 0, top:50, right: 200, bottom:90]`

if you use `.animate().top(50, 0)`

The final values of the Rect will be `[left: 0, top:50, right: 200, bottom:90]` then `[left: 0, top:0, right: 200, bottom:90]`,
**it will not change the bottom of the rect**

<img src="medias/example/top.gif"/>

-----

#### .topTo(values)

It's the same as `top` except it automatically set the first value as the current value

For example, for a Rect `[left: 0, top:50, right: 200, bottom:90]`

if you use `.animate().topTo(0)`, it will animate the top from `50` to `0`

-----

#### .moveTopTo(values)

This method will change the shape `top` value, **keeping the shape height**

For example, for a Rect `[left: 0, top:10, right: 200, bottom:90]`, the height is **80**

if you use `.animate().moveTop(0)`

The final values of the Rect will be `[left: 0, top:0, right: 200, bottom:80]`,
**it will also change the bottom of the rect to keep the height of 80**


<img src="medias/example/move_top_to.gif"/>
-----

#### .topBy(values)

This method will change the shape `top` value, **ignoring its previous height**

**Multiplying** his top by the values

For example, for a Rect `[left: 0, top:10, right: 200, bottom:90]`

if you use `.animate().topBy(0, 0.5, 1f)`

The values of the Rect will be

`[left: 0, top:0, right: 200, bottom:90]` then

`[left: 0, top:5, right: 200, bottom:90]` then

`[left: 0, top:10, right: 200, bottom:90]`

**it will not change the bottom of the rect**

<img src="medias/example/top_by.gif"/>

-----

#### .topPlus(values)

This method will change the shape `top` value, **ignoring its previous height**

**Adding** his top by the values

For example, for a Rect `[left: 0, top:10, right: 200, bottom:90]`

if you use `.animate().topPlus(0, 10, 0)`

The values of the Rect will be

`[left: 0, top:10, right: 200, bottom:90]` then

`[left: 0, top:20, right: 200, bottom:100]` then

`[left: 0, top:10, right: 200, bottom:90]`


# Event handling

Follow the example of [Tree View](entry/src/main/java/canvastoolbox/florent37/github/com/canvastoolbox/views/MyTreeView.java)

User events handling like *onClick* or *onTouch* are handled by an instance of `ShapeEventManager` attached to your component

```java
private final ShapeEventManager shapeAnimator = new ShapeEventManager(this);
```

## Listen click event

You can listen for a shape click using `shapeEventManager.ifClicked(shape, listener)`

```java
shapeEventManager.ifClicked(myShape, clickedShape -> {
    //your action
    myShape.setColor(Color.BLACK);
});
```
<img src="medias/example/on_click.gif"/>

## Listen touch

To handle easier the onTouchEvent, use `shapeEventManager.ifClicked(shape, touchSetup)`

The touchSetup gives you an item `EventHelper`, which help you bind an action with an user interaction

### Custom Actions

Use `eventHelper.onDown((event) -> {/* code */})` to execute a custom action to execute on user finger down

Use `eventHelper.onMove((event) -> {/* code */})` to execute a custom action to execute on user finger move

Use `eventHelper.onUp((event) -> {/* code */})` to execute a custom action to execute on user finger up

### Bound Actions

EventHelper can automatically bind a shape action to an user interaction

For example, you can move move the *CenterX* of a shape to the event.x using

```java
.move(myRectShape, RectShape.Pos.CENTER_X, EventPos.X)
```

Please look at implemented `RectShape.Pos` and `CircleShape.Pos`

<img src="medias/example/sample_tree.gif"/>


# License

    Copyright 2018 florent37, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
