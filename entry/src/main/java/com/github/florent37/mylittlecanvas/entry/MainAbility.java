package com.github.florent37.mylittlecanvas.entry;

import com.github.florent37.mylittlecanvas.entry.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        getWindow().setStatusBarColor(0xff3F51B5);
    }
}
