package canvastoolbox.florent37.github.com.canvastoolbox.graph;

import com.github.florent37.mylittlecanvas.shape.LineShape;
import com.github.florent37.mylittlecanvas.shape.RectShape;
import com.github.florent37.mylittlecanvas.shape.TextShape;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

import static com.github.florent37.mylittlecanvas.CanvasHelper.dpToPx;

public class BarGraph extends Component implements Component.DrawTask, Component.LayoutRefreshedListener {

    private final List<Column> columns = new ArrayList<>();
    private final LineShape axis = new LineShape();

    public BarGraph(Context context) {
        super(context);
        init();
    }

    public BarGraph(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public BarGraph(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        //lets imagine we have 3 bars, and the second bar has 2 setps

        //axis
        axis.setColor(0xFF3E3E3E)
                .setStrokeWidth(dpToPx(this, 2));

        //1st bar
        final Column firstColumn = new Column();

        firstColumn.label
                .setColor(0xFF3E3E3E)
                .setTextSizePx(dpToPx(this, 12))
                .setText("2016");

        firstColumn.rows.add(new RectShape()
                .setColor(0xFF039BE5)
                //we save a variable in the shape,
                //it will be used on `onSizeChanged`
                //because we need the view height/width to set Left/Right/Top/Bottom

                //eg: the value displayed by this bar
                .setVariable("value_percent", 0.20f));

        //2nd bar
        final Column secondColumn = new Column();

        secondColumn.label
                .setColor(0xFF3E3E3E)
                .setTextSizePx(dpToPx(this, 12))
                .setText("2017");
        secondColumn.rows.add(new RectShape()
                .setColor(0xFFFFB300)
                .setVariable("value_percent", 0.40f));
        secondColumn.rows.add(new RectShape()
                .setColor(0xFF6D4C41)
                .setVariable("value_percent", 0.30f));

        //3rd bar
        final Column thirdColumn = new Column();

        thirdColumn.label
                .setColor(0xFF3E3E3E)
                .setTextSizePx(dpToPx(this, 12))
                .setText("2018");
        thirdColumn.rows.add(new RectShape()
                .setColor(0xFFD32F2F)
                .setVariable("value_percent", 0.40f));

        columns.add(firstColumn);
        columns.add(secondColumn);
        columns.add(thirdColumn);

        addDrawTask(this);
        setLayoutRefreshedListener(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        for (Column column : columns) {
            column.label.onDraw(canvas);
            for (RectShape row : column.rows) {
                row.onDraw(canvas);
            }
        }
        axis.onDraw(canvas);
    }

    @Override
    public void onRefreshed(Component component) {
        //here we have the width & height of the view
        //se we can initialise (or update) the rect positions

        int width = component.getWidth();
        int height = component.getHeight();
        final float numberOfColumns = columns.size();
        if (numberOfColumns > 0) {
            final float distanceBetweenColumns = dpToPx(this, 14);
            final float widthOfColumn = (width - (distanceBetweenColumns * (numberOfColumns + 1))) / numberOfColumns;

            final float heightOfLabels = columns.get(0).label.computeTextHeight();
            final float heightOfBars = height - heightOfLabels;

            for (int columnIndex = 0; columnIndex < columns.size(); columnIndex++) {
                final Column column = columns.get(columnIndex);

                //setup the label
                column.label
                        .setHeight(column.label.computeTextHeight())
                        .moveBottomTo(height)
                        .setWidth(widthOfColumn);

                final float previousColumnRight;
                if (columnIndex == 0) {
                    previousColumnRight = 0;
                } else {
                    previousColumnRight = columns.get(columnIndex - 1).label.getRight();
                }

                column.label
                        .moveLeftTo(previousColumnRight)
                        .marginLeft(distanceBetweenColumns);

                //setup the rows
                for (int rowIndex = 0; rowIndex < column.rows.size(); rowIndex++) {
                    final RectShape rectShape = column.rows.get(rowIndex);

                    rectShape.setHeight(heightOfBars * rectShape.<Float>getVariable("value_percent"))
                            .setLeft(column.label.getLeft())
                            .setWidth(widthOfColumn);

                    if (rowIndex == 0) {
                        rectShape.above(column.label)
                                .marginBottom(dpToPx(this, 4));
                    } else {
                        rectShape
                                .above(column.rows.get(rowIndex - 1));
                    }
                }

                final float xAxisY = columns.get(0).label.getTop() - dpToPx(this, 4);
                axis
                        .start(dpToPx(this, 4), xAxisY)
                        .end(width - dpToPx(this, 4), xAxisY);
            }


            invalidate();
        }
    }

    private static class Column {
        private final TextShape label = new TextShape();
        private final List<RectShape> rows = new ArrayList<>();
    }
}
