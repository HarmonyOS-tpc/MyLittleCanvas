package canvastoolbox.florent37.github.com.canvastoolbox.views;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimator;
import com.github.florent37.mylittlecanvas.shape.CircleShape;
import com.github.florent37.mylittlecanvas.shape.LineShape;
import com.github.florent37.mylittlecanvas.shape.RectShape;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class SampleView extends Component implements Component.DrawTask, Component.LayoutRefreshedListener {

    private final RectShape roundRectShape = new RectShape();
    private final CircleShape circleShape = new CircleShape();
    private final LineShape lineShape = new LineShape();

    private final ShapeAnimator shapeAnimator = new ShapeAnimator(this);

    public SampleView(Context context) {
        super(context);
        init();
    }

    public SampleView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public SampleView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        addDrawTask(this);
        setLayoutRefreshedListener(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        //roundRectShape.onDraw(canvas);
        circleShape.onDraw(canvas);
        //lineShape.onDraw(canvas);
    }

    @Override
    public void onRefreshed(Component component) {
        roundRectShape.setColor(Color.WHITE.getValue())

                .setBorderColor(Color.BLUE.getValue())
                .setBorderWidth(15)

                .setCornerRadius(15)
                .setLeft(50)
                .setTop(150)
                .setWidth(200)
                .setHeight(200);

        circleShape
                .setBorderWidth(15)
                .setColor(Color.WHITE.getValue())
                .setBorderColor(Color.BLUE.getValue())
                .setRadius(100)
                .setCenterX(400)
                .setCenterY(400);

        lineShape
                .setColor(Color.BLUE.getValue())
                .setStrokeWidth(10)
                .start(100, 800)
                .end(400, 800);

        setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                circleShape.setColor(Color.BLACK.getValue());
                invalidate();
            }
        });
        invalidate();
    }
}
