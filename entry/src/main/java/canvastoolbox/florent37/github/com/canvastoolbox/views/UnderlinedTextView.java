package canvastoolbox.florent37.github.com.canvastoolbox.views;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimator;
import com.github.florent37.mylittlecanvas.shape.RectShape;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.app.Context;


public class UnderlinedTextView extends Text implements Component.DrawTask, Component.LayoutRefreshedListener {

    final RectShape roundRectShape = new RectShape();
    final ShapeAnimator shapeAnimator = new ShapeAnimator(this);

    public UnderlinedTextView(Context context) {
        this(context, null);
    }

    public UnderlinedTextView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public UnderlinedTextView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        roundRectShape
                .setCornerRadius(10)
                .setColor(0xFF3F51B5);
        addDrawTask(this);
        setLayoutRefreshedListener(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        roundRectShape.onDraw(canvas);
    }

    @Override
    public void onRefreshed(Component component) {
        int w = getWidth();
        int h = getHeight();
        roundRectShape
                .setWidth(w)
                .setHeight(10)
                .alignBottom(h);

        //clear olds anims
        shapeAnimator.clear();

        //start animation
        shapeAnimator
                .setRepeatCount(AnimatorValue.INFINITE)
                .setDuration(1000)
                .play(
                        roundRectShape.animate().left(roundRectShape.getLeft(), roundRectShape.getLeft() + 10, roundRectShape.getLeft()),
                        roundRectShape.animate().right(roundRectShape.getRight(), roundRectShape.getRight() - 10, roundRectShape.getRight())
                )
                .start();
        invalidate();
    }
}
