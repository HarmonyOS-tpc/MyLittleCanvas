package canvastoolbox.florent37.github.com.canvastoolbox.graph;

import com.github.florent37.mylittlecanvas.shape.RectShape;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class AvailableMemorySpace extends Component implements Component.DrawTask, Component.LayoutRefreshedListener {
    //[ ][    ][   ][]

    private final List<RectShape> blocs = new ArrayList<>();

    public AvailableMemorySpace(Context context) {
        super(context);
        init();
    }

    public AvailableMemorySpace(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public AvailableMemorySpace(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(0xFFEEEEEE));
        setBackground(shapeElement);

        //lets imagine we have 8go as memory
        final float memory = 8;

        //we have 3.2 go of apps
        blocs.add(new RectShape()
                .setColor(0xFF039BE5)
                .setVariable("percent", 3.2f * memory / 100f));

        //we have 1.3 go of pictures
        blocs.add(new RectShape()
                .setColor(0xFFD32F2F)
                .setVariable("percent", 1.3f * memory / 100f));

        //we have 2.4 go of musics
        blocs.add(new RectShape()
                .setColor(0xFFFFB300)
                .setVariable("percent", 2.4f * memory / 100f));
        addDrawTask(this);
        setLayoutRefreshedListener(this);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        for (RectShape bloc : blocs) {
            bloc.onDraw(canvas);
        }
    }

    @Override
    public void onRefreshed(Component component) {
        int width = component.getWidth();
        int height = component.getHeight();
        for (int index = 0; index < blocs.size(); index++) {
            final RectShape bloc = blocs.get(index);

            if (index == 0) {
                bloc.setLeft(0);
            } else {
                bloc.toRightOf(blocs.get(index - 1));
            }

            bloc.setTop(0)
                    .setBottom(height)
                    .setWidth(bloc.<Float>getVariable("percent") * width);

        }
        invalidate();
    }
}
