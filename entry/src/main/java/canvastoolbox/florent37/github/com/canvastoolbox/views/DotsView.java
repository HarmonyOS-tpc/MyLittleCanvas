package canvastoolbox.florent37.github.com.canvastoolbox.views;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimator;
import com.github.florent37.mylittlecanvas.shape.CircleShape;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PositionLayout;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.ArrayList;
import java.util.List;

import static com.github.florent37.mylittlecanvas.CanvasHelper.dpToPx;


public class DotsView extends PositionLayout implements Component.DrawTask, Component.LayoutRefreshedListener, Component.BindStateChangedListener {

    private int colors[] = new int[]{
            0xFF039BE5,
            0xFFD32F2F,
            0xFFFFB300,
            0xFF6D4C41
    };

    private int numberOfCircles = 3;
    private List<CircleShape> circles = new ArrayList<>();

    private AnimationHandler animationHandler = new AnimationHandler();

    public DotsView(Context context) {
        super(context);
        init(context);
    }

    public DotsView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DotsView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
//        setWillNotDraw(false);
        addDrawTask(this);
        setLayoutRefreshedListener(this);
        setBindStateChangedListener(this);
    }

    public void animateView() {
        animationHandler.start();
    }

    public void cancelAnimaton() {
        animationHandler.cancel();
    }

    private void animationLoop(final AnimationHandler handler, int animateCircleIndex) {
        final float jump = dpToPx(this, 15);

        final CircleShape firstCircle = circles.get(animateCircleIndex);
        final CircleShape nextCircle = circles.get(animateCircleIndex + 1);

        new ShapeAnimator(this)
                .play(
                        firstCircle.animate().centerXTo(nextCircle.getCenterX()),
                        firstCircle.animate().centerYPlus(-1 * jump, 0),
                        nextCircle.animate().centerXTo(firstCircle.getCenterX())
                )
                .onAnimationEnd(() -> {
                    //intervert circles
                    circles.set(animateCircleIndex, nextCircle);
                    circles.set(animateCircleIndex + 1, firstCircle);

                    handler.next();
                })
                .start();
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        getContext()
                .getUITaskDispatcher()
                .asyncDispatch(
                        () -> animateView());
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        cancelAnimaton();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        for (CircleShape circle : circles) {
            circle.onDraw(canvas);
        }
    }

    @Override
    public void onRefreshed(Component component) {
        int height = component.getHeight();
        if (circles.isEmpty()) {
            for (int i = 0; i < numberOfCircles; i++) {
                circles.add(new CircleShape());
            }
        }

        final float radius = dpToPx(this, 6);
        final float distanceBetweenCircles = dpToPx(this, 16);

        for (int i = 0; i < circles.size(); i++) {
            circles.get(i)
                    .setRadius(radius)
                    .setColor(colors[i % colors.length])
                    .centerVertical(height)
                    .setCenterX((height / 2f) + ((radius + distanceBetweenCircles) * (i - circles.size() / 2)));
        }
        invalidate();
    }

    private class AnimationHandler extends EventHandler {
        private final int CONTINUE_FIRST_ANIM = 1;
        private int animateCircleIndex = 0;

        public AnimationHandler() {
            super(EventRunner.getMainEventRunner());
        }

        public void start() {
            sendEvent(CONTINUE_FIRST_ANIM);
        }

        public void next() {
            animateCircleIndex++;
            if (animateCircleIndex == circles.size() - 1) {
                animateCircleIndex = 0;
            }
            sendEvent(CONTINUE_FIRST_ANIM);
        }

        public void cancel() {
            removeEvent(CONTINUE_FIRST_ANIM);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case CONTINUE_FIRST_ANIM:
                    animationLoop(this, animateCircleIndex);
                    break;
                default:
                    break;
            }
        }
    }
}
