package canvastoolbox.florent37.github.com.canvastoolbox.views;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimator;
import com.github.florent37.mylittlecanvas.shape.CircleShape;
import com.github.florent37.mylittlecanvas.shape.RectShape;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

import static com.github.florent37.mylittlecanvas.CanvasHelper.dpToPx;

public class SwitchView extends Component implements Component.DrawTask, Component.LayoutRefreshedListener {
    private final RectShape background = new RectShape();
    private final CircleShape indicator = new CircleShape();

    private final ShapeAnimator shapeAnimator = new ShapeAnimator(this);

    private boolean checked = false;

    public SwitchView(Context context) {
        super(context);
        init();
    }

    public SwitchView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public SwitchView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                setChecked(!isChecked());
            }
        });
        addDrawTask(this);
        setLayoutRefreshedListener(this);
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        if (checked != this.checked) {
            this.checked = checked;

            final float newCenterX = checked ? getWidth() - indicator.getRadius() : indicator.getRadius();
            shapeAnimator.clear()
                    .play(indicator.animate().centerXTo(newCenterX))
                    .play(indicator.animate().radiusBy(1f, 0.9f, 1f))
                    .start();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        background.onDraw(canvas);
        indicator.onDraw(canvas);

    }

    @Override
    public void onRefreshed(Component component) {
        int w = component.getWidth();
        int h = component.getHeight();
        background
                .setCornerRadius(h / 2f)
                .setColor(0xFFDEDEDE)
                .setRect(0, 0, w, h);

        indicator
                .setColor(Color.WHITE.getValue())
                .setBorderWidth(dpToPx(getContext(), 1))
                .setBorderColor(0xFFDDDDDD)
                .setRadius(h / 2f)
                .centerVertical(h)
                .setCenterX(h / 2f);
        invalidate();
    }
}
