package canvastoolbox.florent37.github.com.canvastoolbox.graph;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.app.Context;

public class LinesGraph extends Component {

    public LinesGraph(Context context) {
        super(context);
    }

    public LinesGraph(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public LinesGraph(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
