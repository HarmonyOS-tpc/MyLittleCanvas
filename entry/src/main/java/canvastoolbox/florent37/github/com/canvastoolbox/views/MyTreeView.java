package canvastoolbox.florent37.github.com.canvastoolbox.views;

import com.github.florent37.mylittlecanvas.shape.LineShape;
import com.github.florent37.mylittlecanvas.shape.RectShape;
import com.github.florent37.mylittlecanvas.shape.TextShape;
import com.github.florent37.mylittlecanvas.touch.EventPos;
import com.github.florent37.mylittlecanvas.touch.ShapeEventManager;
import com.github.florent37.mylittlecanvas.values.Alignment;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class MyTreeView extends Component implements Component.DrawTask, Component.LayoutRefreshedListener {

    final RectShape parent = new RectShape();
    final TextShape textParent = new TextShape();

    final RectShape childLeft = new RectShape();
    final TextShape textChildLeft = new TextShape();
    final LineShape lineParentChildLeft = new LineShape();

    final RectShape childRight = new RectShape();
    final TextShape textChildRight = new TextShape();
    final LineShape lineParentChildRight = new LineShape();

    private ShapeEventManager shapeEventManager = new ShapeEventManager(this);

    private boolean drawInit = false;

    public MyTreeView(Context context) {
        this(context, null);
    }

    public MyTreeView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public MyTreeView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        parent.setCornerRadius(10)
                .setColor(0xFF3F51B5);
        textParent.setColor(Color.WHITE.getValue())
                .setTextSizePx(40);

        childLeft.setCornerRadius(10)
                .setColor(0xFF2196F3);
        textChildLeft.setColor(Color.WHITE.getValue())
                .setTextSizePx(40);
        lineParentChildLeft.setStrokeWidth(3)
                .setColor(0xFF3E3E3E);

        childRight.setCornerRadius(10)
                .setColor(0xFF9575CD);
        textChildRight.setColor(Color.WHITE.getValue())
                .setTextSizePx(40);
        lineParentChildRight.setStrokeWidth(3)
                .setColor(0xFF3E3E3E);

        handleMoving();
        addDrawTask(this);
        setLayoutRefreshedListener(this);
    }

    private void handleMoving() {
        shapeEventManager
                .ifTouched(childLeft, (touchSetup) -> touchSetup
                        .move(childLeft, RectShape.Pos.CENTER_X, EventPos.X)
                        .move(childLeft, RectShape.Pos.TOP, EventPos.Y)
                        .onMove((event) -> textChildLeft.copyPosition(childLeft))
                        .onMove((event) -> lineParentChildLeft.end(childLeft.getCenterX(), childLeft.getTop()))
                )
                //.ifClicked(childLeft, shape -> shape.setColor(Color.BLACK))
                .ifTouched(childRight, (touchSetup) -> touchSetup
                        .move(childRight, RectShape.Pos.CENTER_X, EventPos.X)
                        .move(childRight, RectShape.Pos.TOP, EventPos.Y)
                        .onMove((event) -> textChildRight.copyPosition(childRight))
                        .onMove((event) -> lineParentChildRight.end(childRight.getCenterX(), childRight.getTop()))
                );
    }

    private void init() {
        final int parentWidth = 200;
        parent.setTop(50)
                .setHeight(100)
                .setWidth(parentWidth)
                .centerHorizontal(getWidth());

        textParent.setText("parent")
                .setVerticalAlignment(Alignment.VERTICAL.CENTER)
                .setHorizontalAlignment(Alignment.HORIZONTAL.CENTER)
                .copyPosition(parent);

        final int childWidth = 200;

        childLeft.setLeft(40)
                .setWidth(childWidth)
                .setMinY(parent.getBottom())
                .below(parent)
                .setMinX(0)
                .setMaxX(getWidth())
                .marginTop(250)
                .setHeight(100);

        textChildLeft
                .setText("childLeft")
                .setVerticalAlignment(Alignment.VERTICAL.CENTER)
                .setHorizontalAlignment(Alignment.HORIZONTAL.CENTER)
                .copyPosition(childLeft);

        lineParentChildLeft
                .start(parent.getCenterX(), parent.getBottom())
                .end(childLeft.getCenterX(), childLeft.getTop());

        childRight
                .setLeft(getWidth() - childWidth - 40)
                .setWidth(childWidth)
                .setMinX(0)
                .setMaxX(getWidth())
                .setMinY(parent.getBottom())
                .alignTop(childLeft)
                .setHeight(100);

        textChildRight
                .setText("childRight")
                .setVerticalAlignment(Alignment.VERTICAL.CENTER)
                .setHorizontalAlignment(Alignment.HORIZONTAL.CENTER)
                .copyPosition(childRight);

        lineParentChildRight
                .start(parent.getCenterX(), parent.getBottom())
                .end(childRight.getCenterX(), childRight.getTop());
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        parent.onDraw(canvas);
        textParent.onDraw(canvas);

        childLeft.onDraw(canvas);
        textChildLeft.onDraw(canvas);
        lineParentChildLeft.onDraw(canvas);

        childRight.onDraw(canvas);
        textChildRight.onDraw(canvas);
        lineParentChildRight.onDraw(canvas);
    }

    @Override
    public void onRefreshed(Component component) {
        init();
        invalidate();
    }
}
