package canvastoolbox.florent37.github.com.canvastoolbox.views;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimator;
import com.github.florent37.mylittlecanvas.shape.ArcShape;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.Arrays;
import java.util.List;

import static com.github.florent37.mylittlecanvas.CanvasHelper.dpToPx;

public class ArcView extends Component implements Component.DrawTask, Component.LayoutRefreshedListener, Component.BindStateChangedListener {

    private final ArcShape arcShapeBackground = new ArcShape();
    private final ArcShape arcShapeFooting = new ArcShape();
    private final ArcShape arcShapeCycle = new ArcShape();
    private final ArcShape arcShapeSwimming = new ArcShape();

    private final ShapeAnimator shapeAnimator = new ShapeAnimator(this);

    private final List<ArcShape> arcs = Arrays.asList(arcShapeBackground, arcShapeCycle, arcShapeFooting, arcShapeSwimming);

    private final AnimationHandler animationHandler = new AnimationHandler();

    public ArcView(Context context) {
        super(context);
        init();
    }

    public ArcView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public ArcView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        arcShapeBackground
                .setColor(0xFFEEEEEE)
                .setStartAngle(0)
                .setEndAngle(360);

        arcShapeSwimming
                .setColor(0xFF6599FF)
                .setStartAngle(-90)
                .setEndAngle(30);

        arcShapeFooting
                .setColor(0xFFFF4444)
                .setStartAngle(-90)
                .setEndAngle(120);

        arcShapeCycle
                .setColor(0xFFFF8800)
                .setStartAngle(-90)
                .setEndAngle(280);
        addDrawTask(this);
        setLayoutRefreshedListener(this);
        setBindStateChangedListener(this);
    }

    private void beginAnimation() {

        //for the animation
        shapeAnimator
                .clear()
                .play(
                        arcShapeSwimming.animate().endAngleBy(0f, 1f),
                        arcShapeFooting.animate().endAngleBy(0f, 1f),
                        arcShapeCycle.animate().endAngleBy(0f, 1f)
                )
                .withInitAction(() -> {
                    arcShapeSwimming.setEndAngle(0);
                    arcShapeFooting.setEndAngle(0);
                    arcShapeCycle.setEndAngle(0);
                })
                .setDuration(1400)
                .onAnimationEnd(() -> animationHandler.next())
                .start();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        for (ArcShape arc : arcs) {
            arc.onDraw(canvas);
        }
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        animationHandler.start();
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        animationHandler.cancel();
    }

    @Override
    public void onRefreshed(Component component) {
        int width = component.getWidth();
        int height = component.getHeight();
        final float centerX = width / 2f;
        final float centerY = height / 2f;
        final float radius = Math.min(width, height) / 4f;
        final float strokeWidth = dpToPx(this, 10);

        for (ArcShape arc : arcs) {
            arc.setCenterX(centerX)
                    .setStrokeWidth(strokeWidth)
                    .setCenterY(centerY)
                    .setRadius(radius);
        }
        invalidate();
    }

    private class AnimationHandler extends EventHandler {
        private final int CONTINUE_FIRST_ANIM = 1;

        public AnimationHandler() {
            super(EventRunner.getMainEventRunner());
        }

        public void start() {
            sendEvent(CONTINUE_FIRST_ANIM);
        }

        public void next() {
            sendEvent(CONTINUE_FIRST_ANIM, 4000);
        }

        public void cancel() {
            removeEvent(CONTINUE_FIRST_ANIM);
        }


        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case CONTINUE_FIRST_ANIM:
                    beginAnimation();
                    break;
                default:
                    break;
            }
        }
    }
}
