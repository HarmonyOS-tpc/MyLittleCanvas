package canvastoolbox.florent37.github.com.canvastoolbox.views;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimator;
import com.github.florent37.mylittlecanvas.shape.CircleShape;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import static com.github.florent37.mylittlecanvas.CanvasHelper.dpToPx;

public class RadioView extends Component implements Component.DrawTask, Component.LayoutRefreshedListener {

    private final CircleShape background = new CircleShape();
    private final CircleShape indicator = new CircleShape();

    private final ShapeAnimator shapeAnimator = new ShapeAnimator(this);

    private boolean checked = false;

    public RadioView(Context context) {
        super(context);
        init();
    }

    public RadioView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public RadioView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                setChecked(!isChecked());
            }
        });
        addDrawTask(this);
        setLayoutRefreshedListener(this);
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        if (checked != this.checked) {
            this.checked = checked;

            //retrieve the "variable" named radius_percent ``into indicator
            final float newRadius = checked ? 0 : background.getRadius() * indicator.<Float>getVariable("radius_percent");
            shapeAnimator.clear()
                    .play(indicator.animate().radiusTo(newRadius))
                    .setDuration(200)
                    .setInterpolator(Animator.CurveType.ACCELERATE)
                    .start();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        background.onDraw(canvas);
        indicator.onDraw(canvas);
    }

    @Override
    public void onRefreshed(Component component) {
        int w = component.getWidth();
        int h = component.getHeight();
        final float radius = Math.min(h, w) / 2f;

        background
                .setBorderWidth(dpToPx(this, 2))
                .setBorderColor(0xFFDDDDDD)
                .setColor(0xFFDEDEDE)
                .setRadius(radius)
                .centerVertical(h)
                .centerHorizontal(w);

        //add a "variable" named radius_percent into indicator
        indicator.setVariable("radius_percent", 0.75f);

        indicator
                .setColor(0xFF2E7D32)
                .setBorderWidth(dpToPx(this, 1))
                .setBorderColor(0xFF1B5E20)
                //retrieve the "variable" named radius_percent ``into indicator
                .setRadius(radius * indicator.<Float>getVariable("radius_percent"))
                .centerVertical(h)
                .centerHorizontal(w);
        invalidate();
    }
}
