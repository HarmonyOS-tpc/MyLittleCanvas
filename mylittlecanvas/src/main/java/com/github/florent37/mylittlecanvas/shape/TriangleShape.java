package com.github.florent37.mylittlecanvas.shape;

import ohos.agp.utils.Point;

import java.util.Arrays;
import java.util.List;

public class TriangleShape extends PathShape {

    private final Point point0 = new Point(0f, 0f);
    private final Point point1 = new Point(0f, 0f);
    private final Point point2 = new Point(0f, 0f);
    private final List<Point> points = Arrays.asList(point0, point1, point2);

    @Override
    protected void update() {
        super.update();
        super.path.reset();
        super.path.moveTo(point0.getPointX(), point0.getPointY());
        super.path.lineTo(point1.getPointX(), point1.getPointY());
        super.path.lineTo(point2.getPointX(), point2.getPointY());
        super.path.close();
    }

    public TriangleShape setPoint0(float x, float y) {
        point0.modify(x, y);
        update();
        return this;
    }

    public TriangleShape setPoint1(float x, float y) {
        point1.modify(x, y);
        update();
        return this;
    }

    public TriangleShape setPoint2(float x, float y) {
        point2.modify(x, y);
        update();
        return this;
    }

    public Point getPoint0() {
        return point0;
    }

    public Point getPoint1() {
        return point1;
    }

    public Point getPoint2() {
        return point2;
    }

    /*
    public float getTop() {
        float top = point0.y;
        for (PointF point : points) {
            top = Math.min(top, point.y);
        }
        return top;
    }

    public float getBottom() {
        float bottom = point0.y;
        for (PointF point : points) {
            bottom = Math.max(bottom, point.y);
        }
        return bottom;
    }

    public float getLeft() {
        float left = point0.x;
        for (PointF point : points) {
            left = Math.min(left, point.x);
        }
        return left;
    }

    @Override
    public float getRight() {
        float right = point0.x;
        for (PointF point : points) {
            right = Math.min(right, point.x);
        }
        return right;
    }
    */

    public TriangleShape moveXBy(float differenceX) {
        for (Point point : points) {
            point.position[0] += differenceX;
        }
        update();
        return this;
    }

    public TriangleShape moveCenterXTo(float newCenterX) {
        return this.moveXBy(newCenterX - getCenterX());
    }

    public TriangleShape moveCenterYTo(float newCenterY) {
        return this.moveYBy(newCenterY - getCenterY());
    }

    public TriangleShape moveYBy(float differenceY) {
        for (Point point : points) {
            point.position[1] += differenceY;
        }
        update();
        return this;
    }

    public TriangleShape shadow(float shadowRadius, float shadowDx, float shadowDy, int shadowColor) {
        return (TriangleShape) super.shadow(shadowRadius, shadowDx, shadowDy, shadowColor);
    }

    public TriangleShape setShadowRadius(float shadowRadius) {
        return (TriangleShape) super.setShadowRadius(shadowRadius);
    }

    public TriangleShape setShadowDx(float shadowDx) {
        return (TriangleShape) super.setShadowDx(shadowDx);
    }

    public TriangleShape setShadowDy(float shadowDy) {
        return (TriangleShape) super.setShadowDy(shadowDy);
    }

    public TriangleShape setShadowColor(int shadowColor) {
        return (TriangleShape) super.setShadowColor(shadowColor);
    }
}
