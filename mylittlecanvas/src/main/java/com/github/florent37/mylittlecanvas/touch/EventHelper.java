package com.github.florent37.mylittlecanvas.touch;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimator;
import com.github.florent37.mylittlecanvas.listeners.InvalidateListener;
import com.github.florent37.mylittlecanvas.shape.CircleShape;
import com.github.florent37.mylittlecanvas.shape.RectShape;
import com.github.florent37.mylittlecanvas.shape.Shape;
import com.github.florent37.mylittlecanvas.touch.actions.MoveAction;
import com.github.florent37.mylittlecanvas.touch.actions.MoveActionCircle;
import com.github.florent37.mylittlecanvas.touch.actions.MoveActionRect;
import com.github.florent37.mylittlecanvas.touch.listeners.DownListener;
import com.github.florent37.mylittlecanvas.touch.listeners.MoveListener;
import com.github.florent37.mylittlecanvas.touch.listeners.UpListener;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

public class EventHelper {

    private final InvalidateListener invalidateListener;
    private final List<MoveAction> moveActions = new ArrayList<>();
    private final ShapeAnimator shapeAnimator;
    private final List<EventAnimator> downAnimators = new ArrayList<>();
    private final List<DownListener> downListeners = new ArrayList<>();
    private final List<MoveListener> moveListeners = new ArrayList<>();
    private final List<EventAnimator> upAnimators = new ArrayList<>();
    private final List<UpListener> upListeners = new ArrayList<>();
    boolean startedDown = false;
    private Shape listeningShape;
    private Event waitedEvent = Event.TOUCH;

    public EventHelper(InvalidateListener invalidateListener) {
        this.invalidateListener = invalidateListener;
        shapeAnimator = new ShapeAnimator(invalidateListener);
    }

    // if tapped
    public EventHelper(InvalidateListener invalidateListener, Event waitedEvent, Shape listeningShape) {
        this(invalidateListener);
        this.listeningShape = listeningShape;
        this.waitedEvent = waitedEvent;
    }

    private void postInvalidate() {
        invalidateListener.invalidate();
    }

    public EventHelper onDownAnimate(EventAnimator eventAnimator) {
        downAnimators.add(eventAnimator);
        return this;
    }

    public EventHelper onDown(DownListener listener) {
        downListeners.add(listener);
        return this;
    }

    public EventHelper onMove(MoveListener listener) {
        moveListeners.add(listener);
        return this;
    }

    public EventHelper onUpAnimate(EventAnimator eventAnimator) {
        upAnimators.add(eventAnimator);
        return this;
    }

    public EventHelper onUp(UpListener listener) {
        upListeners.add(listener);
        return this;
    }

    public EventHelper move(CircleShape shape, CircleShape.Pos pos, EventPos eventPos) {
        moveActions.add(new MoveActionCircle(eventPos, shape, pos));
        return this;
    }

    public EventHelper move(RectShape shape, RectShape.Pos pos, EventPos eventPos) {
        moveActions.add(new MoveActionRect(eventPos, shape, pos));
        return this;
    }

    void handleDown(final TouchEvent lastEvent, final TouchEvent event, final Component component) {
        if (accept(lastEvent, event, component)) {
            startedDown = true;
            for (DownListener downListener : downListeners) {
                downListener.onDown(event);
            }
            if (!downAnimators.isEmpty()) {
                shapeAnimator
                        .clear()
                        .play(toAnimators(event, downAnimators))
                        .start();
            }
            postInvalidate();
        }
    }

    void handleMove(final TouchEvent lastEvent, final TouchEvent event, final Component component) {
        if (accept(lastEvent, event, component)) {
            for (MoveAction moveAction : moveActions) {
                moveAction.move(event, component);
            }
            for (MoveListener moveListener : moveListeners) {
                moveListener.onMove(event);
            }
            postInvalidate();
        }
    }

    private List<AnimatorValue> toAnimators(final TouchEvent event, final List<EventAnimator> eventAnimators) {
        final List<AnimatorValue> valueAnimators = new ArrayList<>();
        for (EventAnimator eventAnimator : eventAnimators) {
            valueAnimators.add(eventAnimator.animate(event));
        }
        return valueAnimators;
    }

    void handleUp(final TouchEvent lastEvent, final TouchEvent event, final Component component) {
        if (accept(lastEvent, event, component)) {
            for (UpListener upListener : upListeners) {
                upListener.onUp(event);
            }
            if (!upAnimators.isEmpty()) {
                shapeAnimator
                        .clear()
                        .play(toAnimators(event, upAnimators))
                        .start();
            }
            postInvalidate();
            startedDown = false;
        }
    }

    private boolean accept(TouchEvent lastEvent, TouchEvent event, Component component) {
        if (listeningShape == null) {
            return true;
        } else {
            switch (waitedEvent) {
                case TOUCH:
                    if (event.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                        return listeningShape.containsTouch(getTouchX(event, 0, component), getTouchY(event, 0, component));
                    }
                    return startedDown;
                case CLICK:
                    if (event.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                        return true;
                    }
                    return startedDown && event.getAction() == TouchEvent.PRIMARY_POINT_UP && listeningShape.containsTouch(getTouchX(event, 0, component), getTouchY(event, 0, component));
                default:
                    return false;
            }
        }
    }

    public enum Event {
        TOUCH,
        CLICK,
        DOUBLE_CLICK
    }

    private float getTouchX(TouchEvent touchEvent, int index, Component component) {
        float touchX = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchX = touchEvent.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                touchX = touchEvent.getPointerPosition(index).getX();
            }
        }
        return touchX;
    }

    private float getTouchY(TouchEvent touchEvent, int index, Component component) {
        float touchY = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchY = touchEvent.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                touchY = touchEvent.getPointerPosition(index).getY();
            }
        }
        return touchY;
    }
}
