package com.github.florent37.mylittlecanvas.touch;

import ohos.agp.animation.AnimatorValue;
import ohos.multimodalinput.event.TouchEvent;

public interface EventAnimator {
    AnimatorValue animate(TouchEvent event);
}