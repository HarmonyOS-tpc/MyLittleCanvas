package com.github.florent37.mylittlecanvas.shape;

import ohos.agp.render.Canvas;
import ohos.agp.render.Path;
import ohos.agp.utils.RectFloat;

public class PathShape extends Shape {
    protected Path path = new Path();
    private RectFloat pathBounds = new RectFloat();

    public Path getPath() {
        return path;
    }

    public PathShape setPath(final Path path) {
        this.path.set(path);
        return this;
    }

    @Override
    protected void update() {
        super.update();
        path.computeBounds(pathBounds);
    }

    @Override
    public float getLeft() {
        return pathBounds.left;
    }

    @Override
    public float getTop() {
        return pathBounds.top;
    }

    @Override
    public float getBottom() {
        return pathBounds.bottom;
    }

    @Override
    public float getRight() {
        return pathBounds.right;
    }

    @Override
    public float getHeight() {
        return pathBounds.getHeight();
    }

    @Override
    public float getWidth() {
        return pathBounds.getWidth();
    }

    @Override
    protected void draw(final Canvas canvas) {
        canvas.drawPath(path, paint);
    }

    @Override
    public float getCenterX() {
        return (int) pathBounds.getCenter().getPointX();
    }

    @Override
    public float getCenterY() {
        return (int) pathBounds.getCenter().getPointY();
    }

    @Override
    public boolean containsTouch(float x, float y) {
        return false;
    }
}
