package com.github.florent37.mylittlecanvas.touch.scrollable;

import ohos.agp.components.ComponentContainer;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

public class ScrollableViewGroup implements Scrollable {

    private final Reference<ComponentContainer> viewReference;

    public ScrollableViewGroup(ComponentContainer view) {
        viewReference = new WeakReference<>(view);
    }

    @Override
    public void disableScroll() {
        final ComponentContainer view = viewReference.get();
        if (view != null) {
            view.setEnabled(false);
//            view.requestDisallowInterceptTouchEvent(true);
        }
    }

    @Override
    public void enableScroll() {
        final ComponentContainer view = viewReference.get();
        if (view != null) {
            view.setEnabled(true);
//            view.requestDisallowInterceptTouchEvent(true);
        }
    }
}
