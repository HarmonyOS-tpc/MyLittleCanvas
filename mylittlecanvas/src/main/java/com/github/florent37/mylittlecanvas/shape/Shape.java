package com.github.florent37.mylittlecanvas.shape;

import com.github.florent37.mylittlecanvas.animation.ShapeAnimation;
import ohos.agp.render.BlurDrawLooper;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PathEffect;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;

import java.util.HashMap;
import java.util.Map;

public abstract class Shape {

    //initialise when use it first time, then return it
    protected ShapeAnimation<? extends Shape> shapeShapeAnimation;

    protected final Paint paint = new Paint();

    private boolean willNotDraw = false;

    private float rotation = 0;
    protected Point rotationPivot = null;

    private float scale = 1f;
    protected Point scalePivot = null;

    private float shadowRadius = 0f;
    private float shadowDx = 0f;
    private float shadowDy = 0f;
    private int shadowColor = Color.BLACK.getValue();

    protected float minX = 0;
    protected float maxX = Float.MAX_VALUE;
    protected float minY = 0;
    protected float maxY = Float.MAX_VALUE;

    private Map<String, Object> tags = new HashMap<>();

    public Shape setVariable(String key, Object value) {
        tags.put(key, value);
        return this;
    }

    public <T> T getVariable(String key) {
        return (T) tags.get(key);
    }

    protected Shape() {
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setStrokeWidth(1);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        paint.setFont(Font.DEFAULT);
    }

    public Shape setStyle(Paint.Style style) {
        paint.setStyle(style);
        return this;
    }

    public Shape setStrokeCap(Paint.StrokeCap cap) {
        paint.setStrokeCap(cap);
        return this;
    }

    public Shape setPathEffect(final PathEffect pathEffect) {
        paint.setPathEffect(pathEffect);
        return this;
    }

    public abstract float getHeight();

    public abstract float getWidth();

    public Paint getPaint() {
        return paint;
    }

    public Shape setColor(int color) {
        paint.setColor(new Color(color));
        update();
        return this;
    }

    public Shape setRotation(float rotation) {
        this.rotation = rotation;
        update();
        return this;
    }

    public Shape setScale(float scale) {
        this.scale = scale;
        update();
        return this;
    }

    public float getStrokeWidth() {
        return paint.getStrokeWidth();
    }

    public float getRotation() {
        return rotation;
    }

    public Shape setStrokeWidth(float width) {
        paint.setStrokeWidth(width);
        update();
        return this;
    }

    protected abstract void draw(Canvas canvas);

    public float getShadowRadius() {
        return shadowRadius;
    }

    public Shape setShadowRadius(float shadowRadius) {
        this.shadowRadius = shadowRadius;
        update();
        return this;
    }

    public float getShadowDx() {
        return shadowDx;
    }

    public Shape setShadowDx(float shadowDx) {
        this.shadowDx = shadowDx;
        update();
        return this;
    }

    public float getShadowDy() {
        return shadowDy;
    }

    public Shape setShadowDy(float shadowDy) {
        this.shadowDy = shadowDy;
        update();
        return this;
    }

    public int getShadowColor() {
        return shadowColor;
    }

    public Shape setShadowColor(int shadowColor) {
        this.shadowColor = shadowColor;
        update();
        return this;
    }

    public Shape shadow(float shadowRadius, float shadowDx, float shadowDy, int shadowColor) {
        this.shadowRadius = shadowRadius;
        this.shadowDx = shadowDx;
        this.shadowDy = shadowDy;
        this.shadowColor = shadowColor;
        update();
        return this;
    }

    public void onDraw(Canvas canvas) {
        if (!willNotDraw) {
            canvas.save();
            draw(canvas);
            canvas.restore();
        }
    }

    protected void update() {
        if (shadowRadius != 0) {
            BlurDrawLooper textBlurDrawLooper = new BlurDrawLooper(shadowRadius, shadowDx, shadowDy, new Color(shadowColor));
            paint.setBlurDrawLooper(textBlurDrawLooper);
        } else {
            paint.clearBlurDrawLooper();
        }
    }

    public abstract float getCenterX();

    public abstract float getCenterY();

    public Point getRotationPivot() {
        if (rotationPivot == null) {
            return new Point(getCenterX(), getCenterY());
        }
        return rotationPivot;
    }

    public Point getScalePivot() {
        if (scalePivot == null) {
            return new Point(getCenterX(), getCenterY());
        }
        return scalePivot;
    }

    public void setRotationPivot(float x, float y) {
        if (rotationPivot == null) {
            this.rotationPivot = new Point(x, y);
        } else {
            this.rotationPivot.position[0] = x;
            this.rotationPivot.position[1] = y;
        }
    }

    public void setScalePivot(float x, float y) {
        if (scalePivot == null) {
            this.scalePivot = new Point(x, y);
        } else {
            this.scalePivot.position[0] = x;
            this.scalePivot.position[1] = y;
        }
    }

    public boolean isWillNotDraw() {
        return willNotDraw;
    }

    public Shape setWillNotDraw(boolean willNotDraw) {
        this.willNotDraw = willNotDraw;
        return this;
    }

    public abstract boolean containsTouch(float x, float y);

    public float getScale() {
        return scale;
    }

    public float getAlpha() {
        return paint.getAlpha();
    }

    public Shape setAlpha(final float alpha) {
        paint.setAlpha(alpha);
        update();
        return this;
    }

    public float getMinX() {
        return minX;
    }

    public float getMaxX() {
        return maxX;
    }

    public float getMinY() {
        return minY;
    }

    public float getMaxY() {
        return maxY;
    }

    public Shape setMinX(float minX) {
        this.minX = minX;
        return this;
    }

    public Shape setMaxX(float maxX) {
        this.maxX = (int) maxX;
        return this;
    }

    public Shape setMinY(float minY) {
        this.minY = (int) minY;
        return this;
    }

    public Shape setMaxY(float maxY) {
        this.maxY = (int) maxY;
        return this;
    }

    public ShapeAnimation animate() {
        if (shapeShapeAnimation == null) {
            shapeShapeAnimation = new ShapeAnimation<>(this);
        }
        return shapeShapeAnimation;
    }

    public int getColor() {
        return paint.getColor().getValue();
    }

    public abstract float getLeft();

    public abstract float getTop();

    public abstract float getBottom();

    public abstract float getRight();
}
