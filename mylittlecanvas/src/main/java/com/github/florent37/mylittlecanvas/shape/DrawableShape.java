package com.github.florent37.mylittlecanvas.shape;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.media.image.PixelMap;

public class DrawableShape extends RectShape {

    private Element drawable;

    public DrawableShape setDrawable(final Element drawable) {
        this.drawable = drawable;
        return this;
    }

    public DrawableShape setBitmap(final PixelMap bitmap) {
        this.drawable = new PixelMapElement(bitmap);
        return this;
    }

    @Override
    protected void draw(final Canvas canvas) {
        drawable.setBounds(
                (int) rectF.left,
                (int) rectF.top,
                (int) rectF.right,
                (int) rectF.bottom
        );
        drawable.drawToCanvas(canvas);
    }
}
