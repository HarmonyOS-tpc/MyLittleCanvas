package com.github.florent37.mylittlecanvas.animation;

import com.github.florent37.mylittlecanvas.shape.TriangleShape;
import ohos.agp.animation.AnimatorValue;

public class TriangleShapeAnimation extends ShapeAnimation<TriangleShape> {

    public TriangleShapeAnimation(TriangleShape shape) {
        super(shape);
    }

    public AnimatorValue centerX(float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.moveCenterXTo(AnimatorValueUtils.getAnimatedValue(v, values));
            }
        });
        return animatorValue;
    }

    public AnimatorValue centerXTo(float... values) {
        return centerX(insertAtFirst(shape.getCenterX(), values));
    }

    public AnimatorValue centerXPlus(float... values) {
        final float[] newValues = new float[values.length];
        final float centerX = shape.getCenterX();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] + centerX;
        }
        return centerXTo(newValues);
    }

    public AnimatorValue centerY(float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.moveCenterXTo(AnimatorValueUtils.getAnimatedValue(v, values));
            }
        });
        return animatorValue;
    }

    public AnimatorValue centerYTo(float... values) {
        return centerY(insertAtFirst(shape.getCenterY(), values));
    }

    public AnimatorValue centerYPlus(float... values) {
        final float[] newValues = new float[values.length];
        final float centerY = shape.getCenterY();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] + centerY;
        }
        return centerYTo(newValues);
    }

}
