package com.github.florent37.mylittlecanvas.touch.listeners;


import ohos.multimodalinput.event.TouchEvent;

public interface DownListener {
    void onDown(TouchEvent event);
}
