package com.github.florent37.mylittlecanvas.touch.listeners;

import ohos.multimodalinput.event.TouchEvent;

public interface MoveListener {
    void onMove(TouchEvent event);
}
