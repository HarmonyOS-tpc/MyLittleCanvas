package com.github.florent37.mylittlecanvas.listeners;

import ohos.agp.components.Component;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

public class ViewInvalidateListener implements InvalidateListener {
    private Reference<Component> viewReference;

    public ViewInvalidateListener(Component view) {
        this.viewReference = new WeakReference<Component>(view);
    }

    @Override
    public void invalidate() {
        final Component view = viewReference.get();
        if (view != null) {
            view.invalidate();
        }
    }
}