package com.github.florent37.mylittlecanvas.animation;


import com.github.florent37.mylittlecanvas.shape.CircleShape;
import ohos.agp.animation.AnimatorValue;

public class CircleShapeAnimation extends ShapeAnimation<CircleShape> {

    public CircleShapeAnimation(CircleShape shape) {
        super(shape);
    }

    public AnimatorValue centerX(float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.setCenterX(AnimatorValueUtils.getAnimatedValue(v, values));
            }
        });
        return animatorValue;
    }

    public AnimatorValue centerXTo(float... values) {
        return centerX(insertAtFirst(shape.getCenterX(), values));
    }

    public AnimatorValue centerXPlus(float... values) {
        final float[] newValues = new float[values.length];
        final float centerX = shape.getCenterX();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] + centerX;
        }
        return centerXTo(newValues);
    }

    public AnimatorValue centerY(float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.setCenterY(AnimatorValueUtils.getAnimatedValue(v, values));
            }
        });
        return animatorValue;
    }

    public AnimatorValue centerYTo(float... values) {
        return centerY(insertAtFirst(shape.getCenterY(), values));
    }

    public AnimatorValue centerYPlus(float... values) {
        final float[] newValues = new float[values.length];
        final float centerY = shape.getCenterY();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] + centerY;
        }
        return centerYTo(newValues);
    }

    public AnimatorValue radius(float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.setRadius(AnimatorValueUtils.getAnimatedValue(v, values));
            }
        });
        return animatorValue;
    }

    public AnimatorValue radiusTo(float... values) {
        return radius(insertAtFirst(shape.getRadius(), values));
    }

    public AnimatorValue radiusBy(float... values) {
        final float[] newValues = new float[values.length];
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] * shape.getRadius();
        }
        return radius(newValues);
    }

    public AnimatorValue radiusPlus(float... values) {
        final float[] newValues = new float[values.length];
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] + shape.getRadius();
        }
        return radiusTo(newValues);
    }

    public AnimatorValue borderColorTo(final int... color) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.setBorderColor(AnimatorValueUtils.getAnimatedColor(v, insertAtFirst(shape.getBorderColor(), color)));
            }
        });
        return animatorValue;
    }

    public AnimatorValue borderWidthTo(final float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.setBorderWidth(AnimatorValueUtils.getAnimatedValue(v, insertAtFirst(shape.getBorderWidth(), values)));
            }
        });
        return animatorValue;
    }

}
