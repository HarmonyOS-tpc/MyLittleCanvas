package com.github.florent37.mylittlecanvas.animation;

import com.github.florent37.mylittlecanvas.shape.ArcShape;
import ohos.agp.animation.AnimatorValue;

public class ArcShapeAnimation extends RectShapeAnimation<ArcShape> {
    public ArcShapeAnimation(ArcShape shape) {
        super(shape);
    }

    //region startangle
    public AnimatorValue startAngle(float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.setStartAngle(AnimatorValueUtils.getAnimatedValue(v, values));
            }
        });
        return animatorValue;
    }

    public AnimatorValue startAngleTo(final float... values) {
        return startAngle(insertAtFirst(shape.getStartAngle(), values));
    }

    public AnimatorValue startAngleBy(float... values) {
        final float[] newValues = new float[values.length];
        final float left = shape.getStartAngle();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] * left;
        }
        return startAngle(newValues);
    }

    public AnimatorValue startAnglePlus(float... values) {
        final float[] newValues = new float[values.length];
        final float left = shape.getStartAngle();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] + left;
        }
        return startAngleTo(newValues);
    }
    //endregion

    //region startangle
    public AnimatorValue endAngle(float... values) {
        final AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                shape.setEndAngle(AnimatorValueUtils.getAnimatedValue(v, values));
            }
        });
        return animatorValue;
    }

    public AnimatorValue endAngleTo(final float... values) {
        return endAngle(insertAtFirst(shape.getStartAngle(), values));
    }

    public AnimatorValue endAngleBy(float... values) {
        final float[] newValues = new float[values.length];
        final float left = shape.getEndAngle();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] * left;
        }
        return endAngle(newValues);
    }

    public AnimatorValue endAnglePlus(float... values) {
        final float[] newValues = new float[values.length];
        final float left = shape.getEndAngle();
        for (int i = 0; i < values.length; i++) {
            newValues[i] = values[i] + left;
        }
        return endAngleTo(newValues);
    }
    //endregion
}
