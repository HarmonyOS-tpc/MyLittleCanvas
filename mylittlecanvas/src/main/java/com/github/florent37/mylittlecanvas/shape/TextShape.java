package com.github.florent37.mylittlecanvas.shape;

import com.github.florent37.mylittlecanvas.values.Alignment;
import ohos.agp.render.Canvas;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

public class TextShape extends RectShape {

    private CharSequence text = "";

    private Alignment.VERTICAL verticalAlignment = Alignment.VERTICAL.CENTER;
    private Alignment.HORIZONTAL horizontalAlignment = Alignment.HORIZONTAL.CENTER;

    public TextShape() {
    }

    @Override
    public TextShape setColor(int color) {
        super.setColor(color);
        this.paint.setColor(new Color(color));
        update();
        return this;
    }

    public TextShape setVariable(String key, Object value) {
        return (TextShape) super.setVariable(key, value);
    }


    @Override
    protected void update() {
        super.update();
    }

    public TextShape setTextSizePx(float textSize) {
        paint.setTextSize((int) textSize);
        update();
        return this;
    }

    public float getTextSize() {
        return paint.getTextSize();
    }

    public float computeTextHeight() {
        Rect rect = paint.getTextBounds(text.toString());
        return rect.getHeight();
    }

    public TextShape setTypeface(Font font) {
        paint.setFont(font);
        update();
        return this;
    }

    @Override
    protected void draw(Canvas canvas) {
        if (text == null || text.length() == 0) {
            return;
        }
        final int saveState = canvas.save();
        final float textHeight = calculateHeight();
        final float textWidth = paint.measureText(text.toString());
        if (verticalAlignment == Alignment.VERTICAL.CENTER) {
            canvas.translate(getLeft(), getCenterY() - textHeight / 2f);
        } else if (verticalAlignment == Alignment.VERTICAL.TOP) {
            canvas.translate(getLeft(), getTop());
        } else if (verticalAlignment == Alignment.VERTICAL.BOTTOM) {
            canvas.translate(getLeft(), getBottom() - textHeight);
        }

        if (horizontalAlignment == Alignment.HORIZONTAL.CENTER) {
            canvas.drawText(paint, text.toString(), getWidth() / 2f - textWidth / 2f, textHeight * 0.75f);
        } else if (horizontalAlignment == Alignment.HORIZONTAL.LEFT) {
            canvas.drawText(paint, text.toString(), 0, textHeight * 0.75f);
        } else if (horizontalAlignment == Alignment.HORIZONTAL.RIGHT) {
            canvas.drawText(paint, text.toString(), getWidth() - textWidth, textHeight * 0.75f);
        }
        canvas.restoreToCount(saveState);
    }

    @Override
    public boolean containsTouch(float x, float y) {
        return false;
    }

    public TextShape setHorizontalAlignment(final Alignment.HORIZONTAL alignment) {
        this.horizontalAlignment = alignment;
        update();
        return this;
    }

    public CharSequence getText() {
        return text;
    }

    public TextShape setText(CharSequence text) {
        this.text = text;
        update();
        return this;
    }

    public float calculateHeight() {
        if (text == null || ("" + text).trim().isEmpty()) {
            return 0;
        } else {
            return paint.getTextSize();
        }
    }

    public TextShape setVerticalAlignment(final Alignment.VERTICAL verticalAlignment) {
        this.verticalAlignment = verticalAlignment;
        update();
        return this;
    }
}
