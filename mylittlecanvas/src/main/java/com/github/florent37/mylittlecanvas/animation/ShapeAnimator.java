package com.github.florent37.mylittlecanvas.animation;

import com.github.florent37.mylittlecanvas.listeners.InvalidateListener;
import com.github.florent37.mylittlecanvas.listeners.ViewInvalidateListener;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ShapeAnimator {
    private InvalidateListener invalidateListener;
    private List<AnimatorValue> animators = new ArrayList<>();
    private int repeatCount = 0;
    private long duration = 300;
    private long startDelay = 0;

    private int interpolator = Animator.CurveType.LINEAR;

    private List<InitAction> initActions = new ArrayList<>();
    private List<OnAnimationStart> onAnimationStarts = new ArrayList<>();
    private List<OnAnimationEnd> onAnimationEnds = new ArrayList<>();
    private AtomicInteger endedAnimationsCount = new AtomicInteger(0);
    private boolean started = false;

    public ShapeAnimator(InvalidateListener invalidateListener) {
        this.invalidateListener = invalidateListener;
    }

    public ShapeAnimator(InvalidateListener invalidateListener, List<AnimatorValue> animators) {
        this(invalidateListener);
        play(animators);
    }

    public ShapeAnimator(InvalidateListener invalidateListener, AnimatorValue... animators) {
        this(invalidateListener);
        play(animators);
    }

    public ShapeAnimator(Component view) {
        this(new ViewInvalidateListener(view));
    }

    public ShapeAnimator(Component view, List<AnimatorValue> animators) {
        this(new ViewInvalidateListener(view), animators);
    }

    public ShapeAnimator(Component view, AnimatorValue... animators) {
        this(new ViewInvalidateListener(view), animators);
    }

    public ShapeAnimator clear() {
        for (AnimatorValue animator : animators) {
            animator.cancel();
        }
        initActions.clear();
        onAnimationStarts.clear();
        onAnimationEnds.clear();
        animators.clear();
        started = false;
        return this;
    }

    public ShapeAnimator play(List<AnimatorValue> animators) {
        this.animators.addAll(animators);
        return this;
    }

    public ShapeAnimator play(AnimatorValue... animators) {
        if (animators != null) {
            this.animators.addAll(Arrays.asList(animators));
        }
        return this;
    }

    public ShapeAnimator setInterpolator(int interpolator) {
        this.interpolator = interpolator;
        return this;
    }

    public ShapeAnimator start(final OnAnimationEnd onAnimationEnd) {
        this.onAnimationEnd(onAnimationEnd);
        return start();
    }

    public ShapeAnimator start() {
        if (!started) {
            started = true;
            endedAnimationsCount.set(0);

            for (OnAnimationStart onAnimationStart : onAnimationStarts) {
                onAnimationStart.onAnimationStart();
            }
            //do not use AnimatorSet because you cannot use setRepeatCount

            final int animationCount = animators.size();

            for (AnimatorValue animator : animators) {
                animator.setLoopedCount(repeatCount);
                animator.setDuration(duration);
                animator.setCurveType(interpolator);
                animator.setDelay(startDelay);
//                animator.setValueUpdateListener((animatorValue, v) -> {
//                    if(invalidateListener != null){
//                        invalidateListener.invalidate();
//                    }
//                });
                animator.setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {
                    }

                    @Override
                    public void onStop(Animator animator) {
                    }

                    @Override
                    public void onCancel(Animator animator) {
                    }

                    @Override
                    public void onEnd(Animator animator) {
                        final int finishedAnims = endedAnimationsCount.incrementAndGet();
                        if (animationCount == finishedAnims) {
                            for (OnAnimationEnd onAnimationEnd : onAnimationEnds) {
                                onAnimationEnd.onAnimationEnd();
                            }
                        }
                    }

                    @Override
                    public void onPause(Animator animator) {
                    }

                    @Override
                    public void onResume(Animator animator) {
                    }
                });

                for (InitAction initAction : initActions) {
                    initAction.initAction();
                }

                animator.start();

                AnimatorValue refreshAnimatorValue = new AnimatorValue();
                refreshAnimatorValue.setLoopedCount(repeatCount);
                refreshAnimatorValue.setDuration(duration);
                refreshAnimatorValue.setCurveType(interpolator);
                refreshAnimatorValue.setDelay(startDelay);
                refreshAnimatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        if (invalidateListener != null) {
                            invalidateListener.invalidate();
                        }
                    }
                });
                refreshAnimatorValue.start();
            }
        }
        return this;
    }

    public ShapeAnimator setRepeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
        return this;
    }

    public ShapeAnimator setDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public ShapeAnimator setStartDelay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    public ShapeAnimator onAnimationEnd(final OnAnimationEnd onAnimationEnd) {
        if (onAnimationEnd != null) {
            this.onAnimationEnds.add(onAnimationEnd);
        }
        return this;
    }

    public ShapeAnimator onAnimationStart(final OnAnimationStart onAnimationStart) {
        if (onAnimationStart != null) {
            this.onAnimationStarts.add(onAnimationStart);
        }
        return this;
    }

    public ShapeAnimator withInitAction(InitAction initAction) {
        if (initAction != null) {
            this.initActions.add(initAction);
        }
        return this;
    }

    public interface OnAnimationStart {
        void onAnimationStart();
    }

    public interface InitAction {
        void initAction();
    }

    public interface OnAnimationEnd {
        void onAnimationEnd();
    }
}
